//Include Qtgui module, because application is graphical.
#include <QtGui>
//Include QtWebKit, because application needs a web browser engine.
#include <QtWebKit>
//Include QWebView, because application needs to be able to view web pages.
#include <QWebView>
//Create main function (the function called when the program is run) with argc and argv (so commandline arguments can be passed).
int main(int argc, char *argv[])
{
//Set QApplication to variable "app" with both argc and argv.
	QApplication app(argc, argv);
//Use QStringList to create a variable named "options" with an array of values from argv.
	QStringList options = app.arguments();
//Set QWebView as variable named "view".
	QWebView view;
//Set the browser window title to the value of options[1]. If no value is present, exit with status code 1. [Currently exit causes segmentation fault]
	if (options[1].isEmpty()) {
		QCoreApplication::exit(1);
	} else {
		view.setWindowTitle(options[1]);
	}
//Set the browser url to the value of options[2]. If no value is present, exit with status code 1. [Currently exit causes segmentation fault]
	if (options[2].isEmpty()) {
		QCoreApplication::exit(1);
	} else {
		view.setUrl(QUrl(options[2]));
	}
//Open QWebView (so the webpage can be seen)
	view.show();
//End function
	return app.exec();
}
